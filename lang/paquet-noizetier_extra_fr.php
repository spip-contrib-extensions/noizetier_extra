<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noizetier_extra_nom' => 'NoiZetier : compléments',
	'noizetier_extra_slogan' => 'Des compléments pour le noiZetier',
	'noizetier_extra_description' => 'Ce plugin ajoute des fonctionnalités expérimentales au Noizetier, ayant vocation a y être intégrées.',
);
